package cmd

import (
	"context"
	"testing"

	api "gitlab.com/bloom-flare/string_service/api"
)

type TestCaseCount struct {
	input    string
	expected int32
}

var countTestCases = []TestCaseCount{
	{
		input:    "test",
		expected: 4,
	},
	{
		input:    "",
		expected: 0,
	},
}

func TestCountValid(t *testing.T) {
	s := StringServiceServer{}

	for _, testCase := range countTestCases {
		req := &api.CountRequest{
			Input: testCase.input,
		}

		resp, err := s.Count(context.Background(), req)

		if err != nil {
			t.Errorf("Count test got unexpected error %s", err)
		}

		if resp.Count != testCase.expected {
			t.Errorf("TestCountValid have %d, wanted %d", resp.Count, testCase.expected)
		}
	}
}
