package main

import (
	"context"
	"time"

	glg "github.com/kpango/glg"
	grpc "google.golang.org/grpc"
)

func loggerInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	start := time.Now()

	// Call the handler
	h, err := handler(ctx, req)

	// Logging with GLG
	glg.Infof("Request - Method:%s\tDuration:%s\tError:%v\n", info.FullMethod, time.Since(start), err)

	return h, err
}
