# String Service

[![pipeline status](https://gitlab.com/bloom-flare/string_service/badges/master/pipeline.svg)](https://gitlab.com/bloom-flare/string_service/commits/master)
[![coverage report](https://gitlab.com/bloom-flare/string_service/badges/master/coverage.svg)](https://gitlab.com/bloom-flare/string_service/commits/master)

String Service provides operations on strings over gRPC with TLS encryption.

## This service provides this functions

- [Uppercase function](https://gitlab.com/bloom-flare/string_service/wikis/Uppercase-function)
- [Count function](https://gitlab.com/bloom-flare/string_service/wikis/Count-function)

## Getting started tutorial

You can find a detailed getting started guide in the [wiki home page](https://gitlab.com/bloom-flare/string_service/wikis/Home)