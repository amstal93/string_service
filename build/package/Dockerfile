# Build executable binary
FROM golang:alpine as builder

LABEL maintainer="David Zarandi <david.zarandi@bloomflare.app>"

# Install all the necessary tools 
RUN apk --update add git protobuf make curl gcc g++

# Update gRPC
RUN go get -u google.golang.org/grpc
RUN go get -u github.com/golang/protobuf/protoc-gen-go
RUN export PATH=$PATH:$GOPATH/bin

# Setup our working directory
WORKDIR /usr/src/app
COPY go.mod go.sum ./

# Get our dependecies
RUN go mod download

COPY . .

# Build our binary
RUN make -C ./scripts build-binary

# Build a small image
FROM scratch as release

# Copy our static executable
COPY --from=builder /go/bin/app /go/bin/app

COPY --from=builder /usr/src/app/server-cert.pem /go/bin/app

COPY --from=builder /usr/src/app/server-key.pem /go/bin/app

ENTRYPOINT [ "/go/bin/app" ]